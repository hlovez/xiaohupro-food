## 小胡菜谱开发文档

已上线的源码为hlfood，该项目源码为重新开发源码。

该项目的源码开发过程同步B站视频，需要的话可以跟着视频一起动手——[小胡Pro](https://space.bilibili.com/326117321)。

本项目中的爬虫链接与之前的项目爬虫基础链接不同，这里采用的最新的官网链接。

- food-menu: 服务端项目
- food-menu-wechat: 微信小程序项目

#### 获取微信小程序openid方法

[官方文档](https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/login/auth.code2Session.html)

~~~java
private String appId = "xxxxxxxxxxxxxx";
private String secretId = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
private String url = "https://api.weixin.qq.com/sns/jscode2session";

public Rs getOpenId(String code){
    String requestUrl = url+"?appid="+appId+"&secret="+secretId+"&js_code="+code+"&grant_type=authorization_code";
    String jsonString = restTemplate.getForObject(requestUrl,String.class);
    JSONObject result = JSONObject.parseObject(jsonString)
    return RsUtil.success("获取openId成功",result);
}
~~~

#### 数据库文件——food_menu.sql

