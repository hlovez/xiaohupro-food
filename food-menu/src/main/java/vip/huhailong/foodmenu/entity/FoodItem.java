package vip.huhailong.foodmenu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @program: food-menu
 * @description: 菜谱项
 * @author: 胡海龙
 * @create: 2022-01-02 13:42:16
 **/
@Data
@AllArgsConstructor
public class FoodItem {
    private String foodName;
    private String detailUrl;
    private String foodImage;
}
