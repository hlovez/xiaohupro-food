package vip.huhailong.foodmenu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @program: food-menu
 * @description: 菜谱步骤
 * @author: 胡海龙
 * @create: 2022-01-02 14:18:22
 **/
@Data
@AllArgsConstructor
public class FoodStep {
    private Integer step;               //步骤
    private String stepDescribe;        //步骤描述
    private String stepDescribeImageUrl;//步骤描述对应图片URL
}
