package vip.huhailong.foodmenu.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @program: food-menu
 * @description:
 * @author: 胡海龙
 * @create: 2022-01-04 20:05:32
 **/
@Data
public class User {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String openId;
    private String createTime;
    private String nickName;

}
