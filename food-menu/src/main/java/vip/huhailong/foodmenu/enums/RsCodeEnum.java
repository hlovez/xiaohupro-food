package vip.huhailong.foodmenu.enums;

/**
 * @program: food-menu
 * @description:
 * @author: 胡海龙
 * @create: 2022-01-03 09:35:15
 **/
public enum RsCodeEnum {

    SUCCESS(200),
    ERROR(500);

    private final Integer code;

    RsCodeEnum(Integer code){
        this.code = code;
    }

    public Integer code(){
        return this.code;
    }
}
