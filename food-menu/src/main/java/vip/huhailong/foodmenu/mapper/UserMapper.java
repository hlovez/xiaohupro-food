package vip.huhailong.foodmenu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import vip.huhailong.foodmenu.entity.User;

/**
 * @program: food-menu
 * @description:
 * @author: 胡海龙
 * @create: 2022-01-04 20:08:16
 **/
public interface UserMapper extends BaseMapper<User> {
}
