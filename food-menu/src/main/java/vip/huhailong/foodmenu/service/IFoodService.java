package vip.huhailong.foodmenu.service;

import vip.huhailong.foodmenu.entity.Rs;

/**
 * @program: food-menu
 * @description: 菜谱接口
 * @author: 胡海龙
 * @create: 2022-01-02 14:05:06
 **/
public interface IFoodService {

    Rs getFoodList(Integer page);

    Rs getFoodDetail(String url);

    Rs getSearchList(String keyword);
}
