package vip.huhailong.foodmenu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import vip.huhailong.foodmenu.entity.Rs;
import vip.huhailong.foodmenu.entity.User;

/**
 * @program: food-menu
 * @description:
 * @author: 胡海龙
 * @create: 2022-01-02 10:51:50
 **/
public interface IUserService extends IService<User> {

    User getUserByOpenId(String openId);

    Rs getOpenId(String code);
}
