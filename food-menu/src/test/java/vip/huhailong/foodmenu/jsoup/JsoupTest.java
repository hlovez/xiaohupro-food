package vip.huhailong.foodmenu.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import vip.huhailong.foodmenu.entity.FoodItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @program: food-menu
 * @description:
 * @author: 胡海龙
 * @create: 2022-01-02 11:13:15
 **/
public class JsoupTest {

    @Test
    public void test() throws IOException {
        Document document = Jsoup.connect("https://so.meishi.cc/?q=蛋炒饭").get();
        Elements select = document.select(".search2015_cpitem_w > .search2015_cpitem");
        List<FoodItem> foodItemList = new ArrayList<>();
        for (Element element : select) {
            String foodName = element.select("a").attr("title");
            String detailUrl = element.select("a").attr("href");
            String imageUrl = element.select("a > img").attr("src");
            FoodItem foodItem = new FoodItem(foodName, detailUrl, imageUrl);
            foodItemList.add(foodItem);
        }
        System.out.println(foodItemList);
    }
}
